package pl.cru.cars;

import org.assertj.core.util.Lists;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebClientTest {

    @Autowired
    WebTestClient webClient;

    @Test
    public void should_find_all_books() {
        webClient.get().uri("/cars")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Car.class)
                .hasSize(99);
    }

    @Test
    public void should_find_one_book_by_id() {
        webClient.get().uri("/cars/2")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("2");
    }

    @Test
    public void for_grafana() {
        WebClient cc = WebClient.builder().baseUrl("http://localhost:7575/cars").build();
        Lists.newArrayList(100, 200, 300, 400, 500, 50, 100, 400, 400, 1000, 2000, 40, 5, 450)
                .forEach(number ->
                        cc.get().uri("/2/delayed/" + number)
                                .retrieve()
                                .bodyToMono(Car.class)
                                .subscribe());
    }

}
